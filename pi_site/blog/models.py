from django.db import models
from django.urls import reverse

from config import core_models


class PublishedPostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Post.Status.PUBLISHED)


class DraftPostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Post.Status.DRAFT)


class DeletedPostManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(status=Post.Status.DELETED)


class Post(core_models.CommonModel):
    title = models.CharField(
        verbose_name="заголовок",
        max_length=256,
    )

    sub_title = models.CharField(
        verbose_name="подзаголовок",
        max_length=256,
    )

    slug = models.SlugField(
        verbose_name="Slug",
    )

    text = models.TextField(
        verbose_name="текст",
    )

    description = models.TextField(
        verbose_name="описание",
    )

    conclusion = models.TextField(
        verbose_name="заключение",
    )

    class Status(models.TextChoices):
        DRAFT = "DR", "Черновик"
        DELETED = "Dl", "Удалено"
        PUBLISHED = "PB", "Опубликовано"

    status = models.CharField(
        max_length=2,
        choices=Status.choices,
        default=Status.DRAFT,
        verbose_name="статус",
    )

    objects = models.Manager()
    published = PublishedPostManager()
    draft = DraftPostManager()
    deleted = DeletedPostManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse("blog:post_detail", kwargs={"slug": self.slug})

    class Meta:
        verbose_name = "пост"
        verbose_name_plural = "посты"
