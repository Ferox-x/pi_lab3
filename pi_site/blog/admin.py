from django.contrib import admin
from django.urls import reverse

from . import models


@admin.register(models.Post)
class PostAdmin(admin.ModelAdmin):
    list_display = ("title", "sub_title", "description", "created_at", "updated_at")
    search_fields = ("title", "sub_title")
    prepopulated_fields = {"slug": ("title",)}
    list_filter = ("created_at", "updated_at")

    def view_on_site(self, obj):
        return reverse("blog:post_detail", kwargs={"slug": obj.slug})
