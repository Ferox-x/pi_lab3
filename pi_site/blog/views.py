from django.views import generic

from . import models


class PostListView(generic.ListView):
    model = models.Post
    queryset = models.Post.published.all()
    template_name = "blog/post_list.html"


class PostDetailView(generic.DetailView):
    model = models.Post
    queryset = models.Post.published.all()
    template_name = "blog/post_detail.html"
